x=9
n=10
p=0.5
pbinom(x, n, p)

# OUTPUT
# [1] 0.9990234
# probability of 9 or less (after all trials)
# 10 trials
# probability of outcome 0.5


